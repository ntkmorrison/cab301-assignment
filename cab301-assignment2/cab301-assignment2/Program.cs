﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.IO;

namespace cab301_assignment2
{
    class Program
    {
        const int NUMBER_OF_TESTS = 100;
        // delegate definition for a function which finds the median.
        public delegate int MedianFunction(int[] A);

        // public field for the operation count.
        public static int operationCount;

        // timer used for finding the execution time.
        public static Stopwatch timer;


        static void Main(string[] args)
        {
            // Test the functions for correctness (nb: they have slightly
            // different behaviour)
            Console.Write("BruteForceMedian correct: ");
            Console.WriteLine(TestMedianFunction(BruteForceMedian, false));

            Console.Write("Median correct: ");
            Console.WriteLine(TestMedianFunction(Median, true));

            // The functions are correct. Set up the CSV writing...
            using (StreamWriter w = new StreamWriter("MedianData.csv"))
            {
                // Write the CSV header.
                w.WriteLine("n,BruteForceMedian OperationCount,BruteForceMedian ExecutionTime,Median OperationCount,Median ExecutionTime");
                // Start at n=100, end at n=100000
                for (int n = 1000; n <= 20000; n += 1000)
                {
                    Console.WriteLine("Current array size: {0}", n);

                    // Run the tests
                    double[] BruteForceMedianData = RunMedianFunction(BruteForceMedian, n, NUMBER_OF_TESTS);
                    double[] MedianData = RunMedianFunction(Median, n, NUMBER_OF_TESTS);

                    // Write to the CSV file
                    String line = String.Format(
                        "{0},{1},{2},{3},{4}",
                        n,
                        BruteForceMedianData[0],
                        BruteForceMedianData[1],
                        MedianData[0],
                        MedianData[1]);
                    w.WriteLine(line);
                    w.Flush();
                }
            }


            //Press key to exit
            Console.WriteLine("Press key to exit.");
            Console.ReadKey();
        }

        /*
         * Generate a random permutation from 1 to count using the specified seed.
         * 
         * Calls to GenerateInputData with the same parameters will generate the same random list. 
         */
        static int[] GenerateInputData(int count, int seed) {
            Random rng = new Random(seed);
            int[] array = Enumerable.Range(1, count).ToArray();
            // Fisher-Yates shuffle.
            for (int i = 0; i <= array.Length - 2; i++) {
                //j ← random integer such that i ≤ j < n (count)
                int j = rng.Next(i, count);
                // exchange A[j] and A[i]
                int temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
            return array;
        }

        /*
         * Run a median function to determine avg operation count and execution time.
         * 
         * Returns a double array, [0]=avg. operation count and [1]=avg. execution time
         * 
         * This uses the same input data for both functions.
         * 
         * Parameters:
         * f: the function to test. Must have the same signature as the MedianFunction delegate.
         * ArraySize: the length of the array to run with.
         * NumberRepeats: how many times to repeat the test (takes the average operation count and execution time of these)
         */
        static double[] RunMedianFunction(MedianFunction f, int ArraySize, int NumberRepeats) {
            // make the timer, but don't start it yet!
            timer = new Stopwatch();
            timer.Reset();

            long totalOperationCount = 0;
            for (int i = 0; i < NumberRepeats; i++)
            {
                // make the input array, using the iteration variable as the seed.
                int[] inputArray = GenerateInputData(ArraySize, i*10);
                // start the timer...
                timer.Start();
                // and perform the function (we actually don't need the return value here)...
                int dummy = f(inputArray);
                // then stop the timer quickly after we're done.
                timer.Stop();
                // add the operation count to the total.
                totalOperationCount += operationCount;
            }
            double averageOperationCount = totalOperationCount / (double)NumberRepeats;
            double averageExecutionTimeus = timer.Elapsed.TotalMilliseconds / NumberRepeats;

            return new double[] { averageOperationCount, averageExecutionTimeus };
        }

        /*
         * Test a median function. Return True if the function passed all the tests,
         * false otherwise.
         * 
         * Parameters:
         * f: the function to test. Must have the same signature as the MedianFunction delegate.
         * PickCentreRight: Determines the desired behaviour when there is an even
         * of items in the array.
         *  - if True, pick the number to the right of the centre position.
         *  - if False, pick the number to the left of the centre position.
         */
        public static Boolean TestMedianFunction(MedianFunction f, Boolean PickCentreRight)
        {
            int[] array;
            int expectedValue;

            // TEST CASE 1: only one element in the array
            array = new int[] { 5 };
            expectedValue = 5;
            if (f(array) != expectedValue)
            {
                Console.WriteLine("Test case 1 failed");
                return false;
            }

            // TEST CASE 2: three elements in the array
            array = new int[] { 7, 8, 9 };
            expectedValue = 8;
            if (f(array) != expectedValue) {
                Console.WriteLine("Test case 2 failed");
                return false;
            }

            // TEST CASE 3: elements are equal
            array = new int[] { 20, 20, 20, 20, 20, 20, 20 };
            expectedValue = 20;
            if (f(array) != expectedValue)
            {
                Console.WriteLine("Test case 3 failed");
                return false;
            }

            // TEST CASE 4: they're not sorted, even number of elements
            array = new int[] { 0, 8, 5, 7, 6, 3, 9, 2 };
            expectedValue = PickCentreRight ? 6 : 5;
            if (f(array) != expectedValue)
            {
                Console.WriteLine("Test case 4 failed");
                return false;
            }

            // TEST CASE 5: negative numbers
            array = new int[] { -4, 2, 5, -9, 4, 1 };
            expectedValue = PickCentreRight ? 2 : 1;
            if (f(array) != expectedValue)
            {
                Console.WriteLine("Test case 5 failed");
                return false;
            }

            // TEST CASE 6: the median shows up multiple times
            array = new int[] { -100, 50, 20, 60, 2104040, 85, 2222, 60 };
            expectedValue = 60;
            if (f(array) != expectedValue)
            {
                Console.WriteLine("Test case 6 failed");
                return false;
            }

            // TEST CASE 7: large array size
            array = new int[] {
                5976, 5269, 2751, 2666, 2355, 3539, 8551, 48, 3009, 6457, 8303, 2034, 609, 1112, 7628, 533, 4304, 3266, 4056, 4840, 8498, 511, 46, 5069, 4300, 273, 5184, 893, 1106, 5264, 3034, 3335, 7627, 2715, 2271, 6307, 3918, 7725, 8039, 9560, 9423, 3071, 8311, 7348, 2013, 3347, 5264, 1378, 3386, 686, 6917, 7618, 6931, 4573, 9872, 2476, 6702, 4057, 9737, 7281, 7354, 676, 431, 2375, 7424, 5929, 743, 9891, 1903, 4199, 5074, 6649, 8184, 3349, 4102, 9549, 6707, 2798, 1514, 3080, 4195, 6132, 2497, 8418, 5636, 7988, 9405, 7857, 3334, 3424, 32, 418, 3171, 1696, 6852, 5012, 5509, 1374, 2407, 3599, 6938, 3858, 4628, 6698, 673, 6691, 6931, 8580, 3851, 9888, 9147, 3525, 4281, 1003, 2776, 3756, 5020, 2361, 4110, 9714, 6471, 3041, 9381, 9644, 1892, 6977, 7289, 2980, 8639, 8513, 200, 5407, 4819, 3184, 9527, 6270, 4818, 6577, 6251, 2758, 2005, 5958, 3419, 8278, 3555, 3791, 1092, 4746, 6740, 45, 4381, 734, 2067, 4270, 9227, 469, 7002, 4923, 5808, 952, 7724, 3009, 5912, 8025, 7178, 504, 9023, 1116, 9857, 5257, 4290, 6721, 6330, 8364, 9929, 3643, 6436, 810, 730, 5336, 4336, 1565, 7323, 7529, 6665, 4072, 2051, 1518, 4845, 743, 9961, 5355, 4767, 8381, 4353, 1754, 3026, 7764, 9293, 9216, 8483, 8175, 1414, 446, 4493, 2340, 5324, 4668, 1420, 1410, 6222, 4131, 7910, 5005, 8029, 4679, 1826, 8490, 3108, 2410, 1719, 3051, 5625, 5529, 7119, 8224, 7504, 6993, 9448, 6341, 2776, 3100, 907, 4570, 5639, 1819, 8932, 1869, 758, 4597, 8822, 6916, 7634, 3992, 9528, 5528, 7010, 48, 3691, 2, 827, 3557, 8484, 2538, 9112, 8070, 4341, 4327, 1272, 4113, 9363, 943, 5287, 2433, 9668, 9582, 8651, 5647, 3187, 4280, 752, 8472, 4973, 7303, 6839, 9583, 2585, 8041, 8564, 1226, 7429, 9116, 2005, 9100, 7807, 3614, 3483, 3370, 340, 6711, 6516, 3142, 6126, 9711, 9906, 198, 4468, 2334, 5753, 7677, 9809, 6796, 2052, 8453, 5215, 8971, 7362, 6469, 9450, 5548, 760, 4169, 3634, 9234, 5649, 6755, 7672, 3551, 4121, 2631, 6735, 7030, 4315, 237, 7419, 2762, 1328, 3979, 7857, 8607, 1883, 5528, 5358, 5616, 9455, 3884, 6388, 4283, 743, 1284, 271, 1995, 9076, 1675, 4890, 1511, 4799, 9158, 5241, 2554, 1092, 6911, 7, 8413, 5747, 5369, 9768, 5055, 9138, 9826, 5628, 2609, 3647, 8927, 156, 2172, 669, 2812, 8180, 535, 8849, 4684, 9380, 4795, 8450, 8113, 8219, 7398, 1487, 9120, 2251, 8637, 4894, 9385, 52, 2986, 3081, 9, 7845, 852, 6335, 2674, 6310, 4652, 4624, 5439, 8848, 8272, 801, 4986, 8532, 5408, 93, 9528, 8715, 9878, 3996, 7486, 4264, 6843, 648, 2558, 1583, 9578, 2144, 3232, 5024, 5741, 4968, 9080, 6526, 7798, 8434, 5226, 8766, 8936, 1986, 517, 8930, 2891, 9736, 5385, 757, 5690, 1679, 944, 4917, 7956, 9240, 3847, 3609, 5447, 2873, 6032, 1336, 1205, 6585, 2960, 5357, 1237, 4730, 5035, 6861, 8980, 7376, 2914, 9779, 8957, 1607, 1358, 6286, 5258, 1463, 5305, 3219, 5248, 8825, 4299, 4663, 9756, 2512, 2911, 4204, 2526, 5980, 7520, 6044, 597, 9638, 4826, 5422, 5648, 6906, 9176, 5703, 8829, 7971, 1187, 8481, 3868, 6274, 3539, 5764, 1307, 8494, 7692, 6180, 7347, 6268, 8368, 1885, 5679, 4599, 1305, 2425, 2398, 6946, 6626, 3218, 6408, 9561, 670, 8258
            };
            expectedValue = 5024;
            if (f(array) != expectedValue)
            {
                Console.WriteLine("Test case 7 failed");
                return false;
            }

            // TEST CASE 8: very large and very small integers
            array = new int[] {
                int.MaxValue, int.MinValue, 0
            };
            expectedValue = 0;
            if (f(array) != expectedValue)
            {
                Console.WriteLine("Test case 8 failed");
                return false;
            }

            // If we survived up to here, all the tests are passed
            return true;
        }

        // We are also turning off any compiler optimisations for these functions.
        [MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
        public static int BruteForceMedian(int[] A)
        {
            // Reset the operation counter.
            operationCount = 0;

            int k = (int)Math.Ceiling(A.Length / 2f);
            for (int i = 0; i <= A.Length - 1; i++)
            {
                int numsmaller = 0;
                int numequal = 0;
                for (int j = 0; j <= A.Length - 1; j++)
                {
                    operationCount++;
                    if (A[j] < A[i])
                    {
                        numsmaller++;
                    }
                    else
                    {
                        operationCount++;
                        if (A[j] == A[i])
                        {
                            numequal++;
                        }
                    }
                }
                if (numsmaller < k && k <= (numsmaller + numequal))
                {
                    return A[i];
                }
            }
            //This value should never be reached.
            return 0;
        }


        // We are also turning off any compiler optimisations for these functions.
        [MethodImpl(MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining)]
        public static int Median(int[] A)
        {
            // Reset the operation counter.
            operationCount = 0;

            if (A.Length == 1)
            {
                return A[0];
            }
            else
            {
                return Select(A, 0, (int)Math.Floor(A.Length / 2f), A.Length - 1);
            }
        }

        public static int Select(int[] A, int l, int m, int h)
        {
            int pos = Partition(A, l, h);
            if (pos == m)
            {
                return A[pos];
            }
            if (pos > m)
            {
                return Select(A, l, m, pos - 1);
            }
            if (pos < m)
            {
                return Select(A, pos + 1, m, h);
            }

            //This value should never be reached.
            return 0;
        }

        public static int Partition(int[] A, int l, int h)
        {
            int pivotval = A[l];
            int pivotloc = l;
            for (int j = l + 1; j <= h; j++)
            {
                // increment operation count.
                operationCount++;
                if (A[j] < pivotval)
                {
                    pivotloc++;
                    Swap(A, pivotloc, j);
                }
            }
            Swap(A, l, pivotloc);
            return pivotloc;
        }

        public static void Swap(int[] A, int i, int j)
        {
            int tempVal = A[i];
            A[i] = A[j];
            A[j] = tempVal;
        }
    }
}
